This module adds a bunch of new arguments to views.  You can now have views
with an argument "Taxonomy: Term name from vocabulary x" for each vocabulary.

The only similar argument provided by the views module is 
"Taxonomy: Term Name", which searches all vocabularies.  This module provides
finer control.

The views module already provides similar functionality for Filters, but this
module adds that functionality for Arguments.

(note:  This was rejected as an enhancement to the views module.  That's why
it lives as its own module.  See bug http://drupal.org/node/279904).
